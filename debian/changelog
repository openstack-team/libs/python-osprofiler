python-osprofiler (4.2.0-4) unstable; urgency=medium

  * Move python3-jaeger-client to recommends (Closes: #1087921).

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Jan 2025 12:55:19 +0100

python-osprofiler (4.2.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090586).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 23:54:43 +0100

python-osprofiler (4.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 00:24:49 +0200

python-osprofiler (4.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-oslo.config as depends.
  * Add python3-jaeger-client as (build-)depends.
  * Blacklist all test_otlp tests that need opentelemetry-{sdk,exporter-otlp}.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Aug 2024 11:08:36 +0200

python-osprofiler (3.4.3-3) unstable; urgency=medium

  * Cleans better (Closes: #1046090).

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Aug 2023 12:40:05 +0200

python-osprofiler (3.4.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 12:04:26 +0200

python-osprofiler (3.4.3-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.
  * Add autopkgtest.
  * Add python3-sphinxcontrib.apidoc as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Aug 2022 12:02:38 +0200

python-osprofiler (3.4.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 11:28:17 +0200

python-osprofiler (3.4.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Sep 2021 11:15:41 +0200

python-osprofiler (3.4.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Aug 2021 10:42:37 +0200

python-osprofiler (3.4.0-3) unstable; urgency=medium

  * Added Restrictions: superficial to d/tests/control (Closes: #974519).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Nov 2020 23:58:47 +0100

python-osprofiler (3.4.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 13:27:46 +0200

python-osprofiler (3.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 11:12:47 +0200

python-osprofiler (3.3.0-1) experimental; urgency=medium

  * Fixed homepage field and watch file.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 17:47:16 +0200

python-osprofiler (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 18:23:19 +0200

python-osprofiler (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 16:41:21 +0200

python-osprofiler (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 22:44:37 +0200

python-osprofiler (2.8.2-3) unstable; urgency=medium

  * Removed Python 2 autopkgtest (Closes: #937988).

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Jan 2020 22:39:09 +0100

python-osprofiler (2.8.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 10:06:21 +0200

python-osprofiler (2.8.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Sep 2019 17:32:39 +0200

python-osprofiler (2.6.0-2) unstable; urgency=medium

  * Removed python3-osprofiler.postinst which had update-alternatives for dual
    Python support (Closes: #940313).

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Sep 2019 11:55:39 +0200

python-osprofiler (2.6.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.
  * Removed Python 2 support (Closes: #937988).

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Sep 2019 08:48:43 +0200

python-osprofiler (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 15:36:20 +0200

python-osprofiler (2.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Build doc with Python 3.
  * Blacklist all test_jaeger test.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 17:36:48 +0200

python-osprofiler (1.11.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 00:18:21 +0000

python-osprofiler (1.11.0-1) experimental; urgency=medium

  [ Corey Bryant ]
  * d/control: Add python-oslo.config to BDs to fix test import error.
  * d/p/drop-sphinx-git.patch, d/control: Drop use of git from sphinx config.

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright: Added myself to Debian part
  * Added simple Debian tests
  * Fix copyright year in documentation to make build reproducible
  * d/rules: Changed UPSTREAM_GIT to new URL
  * d/copyright: Changed source URL to new one

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.
  * Removed all patches, now useless.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Oct 2017 14:03:29 +0200

python-osprofiler (1.2.0-2) unstable; urgency=medium

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 13:13:51 +0000

python-osprofiler (1.2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 22:40:52 +0800

python-osprofiler (0.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed watch file to use github tag rather than broken pypi.
  * Fixed debian/copyright ordering.
  * Standards-Version is now 3.9.6 (no change).
  * Ran wrap-and-sort -t -a.

 -- Thomas Goirand <zigo@debian.org>  Thu, 21 Jan 2016 03:32:01 +0000

python-osprofiler (0.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Removed the no-intersphinx patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Dec 2015 18:08:09 +0100

python-osprofiler (0.3.0-3) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.
  * Added dh-python as b-d.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 23:33:21 +0000

python-osprofiler (0.3.0-2) unstable; urgency=medium

  * Made build reproducible (Closes: #788503). Thanks to Juan Picca.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jun 2015 11:18:04 +0200

python-osprofiler (0.3.0-1) unstable; urgency=medium

  * Initial release. (Closes: #760529)

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Sep 2014 09:38:53 +0800
